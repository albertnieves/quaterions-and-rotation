﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AlignmentScript : MonoBehaviour
{
    public Transform target1;
    public Transform target2;
	public int exercise = 2;

    Quaternion Offset;
    Vector3 axisX;
    float angleX;
	
	
    // Use this for initialization
    void Start ()
    {
        Offset = Quaternion.Inverse(target2.rotation) * target1.rotation;

    }

    float prodEsc(Vector3 V1, Vector3 V2)
    {
        return V1.x * V2.x + V1.y * V2.y + V1.z * V2.z;
    }

    Vector3 prodVect(Vector3 V1, Vector3 V2)
    {
        return new Vector3(V1.y * V2.z - V2.y * V1.z, V2.x * V1.z - V1.x * V2.z, V1.x * V2.y - V1.y * V1.x);
    }

    void Update()
    {

        switch (exercise)
        {
            case 1:
            {
                    axisX = Vector3.Cross(transform.right, target1.right).normalized;

                    angleX = -Mathf.Acos(Vector3.Dot(transform.right, target1.right)) * Mathf.Rad2Deg;

                    target1.Rotate(axisX, angleX, Space.World);



                    Vector3 axisY = Vector3.Cross(transform.up, target1.up).normalized;

                    float angleY = -Mathf.Acos(Vector3.Dot(transform.up, target1.up)) * Mathf.Rad2Deg;

                    target1.Rotate(axisY, angleY, Space.World);

                } break;

          

            case 2:
            {
                    target1.rotation = Quaternion.Slerp(target1.rotation, target2.rotation, 0.1f);
               
            } break;

            case 3:
            {
                    target1.rotation = target2.rotation * Offset;

            } break;

            case 4:
            {
                    target1.rotation = target2.rotation * Offset;
                    target2.rotation = Quaternion.Slerp(transform.rotation, target2.rotation, 0.1f);

                } break;
        }
    }
}
