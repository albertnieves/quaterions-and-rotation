﻿using UnityEngine;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets
{
    class MyQuaternion
    {
        public float x, y, z, w;

        public MyQuaternion(float auxX, float auxY, float auxZ, float auxW)
        {
            x = auxX;
            y = auxY;
            z = auxZ;
            w = auxW;
        }

        public MyQuaternion toMyQuat(Quaternion quat)
        {

            return new MyQuaternion(quat.x, quat.y, quat.z, quat.w); 
        }

        public Quaternion toQuat(MyQuaternion quat)
        {

            return new Quaternion(quat.x, quat.y, quat.z, quat.w);
        }

        public MyQuaternion Product(MyQuaternion quat1, MyQuaternion quat2)
        {
            return new MyQuaternion(
                (quat1.w * quat2.w - quat1.x * quat2.x - quat1.y * quat2.y - quat1.z * quat2.z), 
                (quat1.w * quat2.x + quat1.x * quat2.w + quat1.y * quat2.z - quat1.z * quat2.y),
                (quat1.w * quat2.y - quat1.x * quat2.z + quat1.y * quat2.w + quat1.z * quat2.x),
                (quat1.w * quat2.z - quat1.x * quat2.y + quat1.y * quat2.x + quat1.z * quat2.w));
        }
    }
}
