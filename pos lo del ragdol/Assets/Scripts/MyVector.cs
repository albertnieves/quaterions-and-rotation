﻿using UnityEngine;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets
{
    class MyVector
    {
        public MyVector(float x, float y, float z)
        {

        }

        public float x, y, z;

        public float module()
        {
            return (float)Mathf.Sqrt(Mathf.Pow(x, 2) + Mathf.Pow(y, 2) + Mathf.Pow(z, 2));
        }

        public float escalar(MyVector vector1, MyVector vector2)
        {
            return vector1.x * vector2.x + vector1.y * vector2.y + vector1.z * vector2.z;
        }

        public MyVector vectorial(MyVector vector1, MyVector vector2)
        {
            return new MyVector(vector1.y * vector2.z - vector2.y * vector1.z, vector2.x * vector1.z - vector1.x * vector2.z, vector1.x * vector2.y - vector1.y * vector1.x);
        }
    }
}
