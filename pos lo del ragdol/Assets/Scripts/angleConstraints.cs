﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class angleConstraints : MonoBehaviour
{
    public bool active;

    [Range(0.0f, 180.0f)]
    public float maxAngle;

    [Range(0.0f, 180.0f)]
    public float minAngle;

    public Transform parent;
    public Transform child;
    public Transform plane;
    Vector3 arm, projPlane, toRotate;

    void Start()
    {
        

    }

    void Update()
    {
        if (active)
        {
            arm = parent.localPosition - plane.localPosition;
            projPlane = Vector3.ProjectOnPlane(arm, plane.up);
            toRotate = Vector3.Cross(arm, projPlane);
            child.Rotate(toRotate);
        }
    }

    private float ComputeAngle(Vector3 ToParent, Vector3 ToChild)
    {
       
        return 0.0f;
    }
}
